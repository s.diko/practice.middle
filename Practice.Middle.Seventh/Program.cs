﻿using Practice.Middle.Seventh;
using static System.Console;

var myList = new MyList<int>() { 0,1,2,3,4 };

for (int i = 5; i < 20; i++)
    myList.Add(i++);

var array = myList.GetArray();

foreach (var item in array)
{
    WriteLine(item);
}