﻿using System.Collections;

namespace Practice.Middle.Seventh;

public class MyList<T> : IEnumerable<T>
{
    private const int defaultCapacity = 4;
    private T[] _items;

    public int Capacity => _items.Length;
    public int Count { get; private set; }

    public MyList()
    {
        _items = new T[defaultCapacity];
    }

    public MyList(IEnumerable<T> items)
    {
        _items = items.ToArray();
        Count = items.Count();
    }

    public void Add(T item)
    {
        if (Count >= Capacity)
        {
            var newArray = new T[Capacity == 0 ? defaultCapacity : Capacity * 2];
            _items.CopyTo(newArray, 0);
            _items = newArray;
        }

        _items[Count] = item;
        Count++;
    }

    public T? this[int index] => Count > index ? _items[index] : default;

    public IEnumerator<T> GetEnumerator()
    {
        foreach (var item in _items.Take(Count))
            yield return item;
    }

    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
}
