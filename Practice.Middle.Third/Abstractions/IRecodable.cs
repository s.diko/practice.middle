﻿namespace Practice.Middle.Third.Abstractions;

public interface IRecodable
{
    void Record();
    void Pause();
    void Stop();
}