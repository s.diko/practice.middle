﻿namespace Practice.Middle.Third.Abstractions;

public interface IPlayable
{
    void Play();
    void Pause();
    void Stop();
}
