﻿namespace Practice.Middle.Third.Models;

public enum PlayerState
{
    Stopped,
    Playing,
    Recording,
    PlayOnPause,
    RecordOnPause,
}
