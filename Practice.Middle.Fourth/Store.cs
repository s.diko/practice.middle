﻿namespace Practice.Middle.Fourth;

public class Store
{
    private readonly Article[] articles;

    public Store(params Article[] articles)
    {
        this.articles = articles;
    }

    public Article? this[int index] => articles.Length > index ? articles?[index] : default;

    /// <summary>
    /// Возвращает товары из всех магазинов по имени
    /// </summary>
    /// <param name="name">Имя товара</param>
    /// <returns>Список товаров</returns>
    public Article[] this[string name] => articles.Where(a => a.ProductName == name).ToArray();
}