﻿#pragma warning disable IDE0032 //Use auto property

namespace Practice.Middle.Fourth;

public class Article
{
    private readonly string productName;
    private readonly string shopName;
    private readonly decimal cost;

    public Article(string productName, string shopName, decimal cost)
    {
        this.productName = productName ?? throw new ArgumentNullException(nameof(productName));
        this.shopName = shopName ?? throw new ArgumentNullException(nameof(shopName));
        this.cost = cost;
    }

    public string ProductName { get => productName; }
    public string ShopName { get => shopName; }
    public decimal Cost { get => cost; }
}
