﻿using Practice.Middle.Second.Handlers;

Console.Write("Enter 0 for open exist file or other for create new: ");
var isNew = Console.ReadLine() != "0";

string fileName = ReadFileName(args, isNew);

using var doc = AbstractHandler.CreateInstance(fileName);

var type = doc switch
{
    XMLHandler => "xml",
    DOCHandler => "doc",
    _ => "txt",
};

doc.Open();
doc.Save();

Console.WriteLine($"Type of file'{fileName}' is {type}");

_ = Console.ReadKey();

static string ReadFileName(string[] args, bool isNew)
{
    if (args.Length > 0 && File.Exists(args[0]))
    {
        return args[0];
    }
    else
    {
        while (true)
        {
            Console.Write("Enter name of file: ");
            var fileName = Console.ReadLine();

            if (fileName is null)
            {
                Console.WriteLine("File name cannoy be empty");
                continue;
            }

            if (isNew || File.Exists(fileName))
            {
                return fileName;
            }
            else
            {
                Console.WriteLine($"File '{fileName}' doesn't exist");
            }
        }
    }
}