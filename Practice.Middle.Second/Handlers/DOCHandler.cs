﻿using DocumentFormat.OpenXml.Packaging;

namespace Practice.Middle.Second.Handlers;

public class DOCHandler : AbstractHandler<WordprocessingDocument>, IDisposable
{
    public DOCHandler(string fileName) : base(fileName) { }

    public override void Create()
    {
        Document = WordprocessingDocument.Create(FileName, DocumentFormat.OpenXml.WordprocessingDocumentType.Document);
    }

    public override void Edit() => Open();

    public override void Open()
    {
        if (Exists)
            Document = WordprocessingDocument.Open(FileName, false);
        else
            Create();
    }

    public override void Save()
    {
        Document?.Save();
        Document?.Close();
    }

    public override void Dispose()
    {
        Document?.Dispose();
        GC.SuppressFinalize(this);
    }
}