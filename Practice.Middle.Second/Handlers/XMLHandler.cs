﻿using System.Xml.Linq;

namespace Practice.Middle.Second.Handlers;

public class XMLHandler : AbstractHandler<XDocument>
{
    public XMLHandler(string fileName) : base(fileName) { }

    public override void Create()
    {
        Document = new();
    }

    public override void Edit() => Open();

    public override void Open() => Document = Exists ? XDocument.Load(FileName) : new();

    public override void Save()
    {
        Document?.Save(FileName);
    }
}
