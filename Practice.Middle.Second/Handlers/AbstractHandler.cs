﻿using MimeDetective;
using MimeDetective.Engine;

namespace Practice.Middle.Second.Handlers;

public abstract class AbstractHandler : IDisposable
{
    protected string FileName { get; init; }
    public bool Exists => File.Exists(FileName);

    protected AbstractHandler(string fileName)
    {
        FileName = fileName;
    }

    public abstract void Open();
    public abstract void Create();
    public abstract void Edit();
    public abstract void Save();

    public static AbstractHandler CreateInstance(string fileName)
    {
        DefinitionMatch? match = default;
        if (File.Exists(fileName))
        {
            var inspector = new ContentInspectorBuilder()
            {
                Definitions = MimeDetective.Definitions.Default.All(),
            }.Build();

            var definitionMatches = inspector.Inspect(ContentReader.Default.ReadFromFile(fileName));
            match = definitionMatches.FirstOrDefault();
        }

        var extension = (match?.Definition.File.Extensions.FirstOrDefault() ?? fileName.Split('.').LastOrDefault())?.Trim(' ', '.');

        return extension switch
        {
            "doc" or "docx" => new DOCHandler(fileName),
            "xml" => new XMLHandler(fileName),
            _ => new TXTHandler(fileName),
        };
    }

    public virtual void Dispose()
    {
        GC.SuppressFinalize(this);
    }
}

public abstract class AbstractHandler<T> : AbstractHandler
{
    public T? Document { get; protected set; }
    protected AbstractHandler(string fileName) : base(fileName)
    {
    }

    public virtual void Edit(Action<T> action)
    {
        if (Document is not null)
            action(Document);
    }

    public virtual void Create(Action<T> action)
    {
        Create();
        Edit(action);
    }
}
