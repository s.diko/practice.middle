﻿namespace Practice.Middle.Second.Handlers;

public class TXTHandler : AbstractHandler<FileStream>, IDisposable
{
    public TXTHandler(string fileName) : base(fileName) { }

    public override void Create()
    {
        Document = new FileStream(FileName, FileMode.Create, FileAccess.Write);
    }

    public override void Edit()
    {
        Document = new FileStream(FileName, FileMode.OpenOrCreate, FileAccess.Write);
    }

    public override void Open()
    {
        Document = new FileStream(FileName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
    }

    public override void Save()
    {
        Document?.Close();
    }

    public override void Dispose()
    {
        Document?.Dispose();
        GC.SuppressFinalize(this);
    }
}
