﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Practice.Middle.Eighth;

public class TimeSpanToStringConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value is not TimeSpan time)
            time = TimeSpan.Zero;
        return time.ToString(@"hh\:mm\:ss\:ff", culture);
    }

    public object? ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return targetType == typeof(TimeSpan)
            ? TimeSpan.TryParse(value?.ToString(), out var time) ? time : TimeSpan.Zero
            : (object?)default;
    }
}